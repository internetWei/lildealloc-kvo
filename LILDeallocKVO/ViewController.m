//
//  ViewController.m
//  LILDeallocKVO
//
//  Created by LL on 2022/7/22.
//

#import "ViewController.h"

#import "NSObject+LILDealloc.h"
#import "Person.h"

@interface ViewController ()

@property (nonatomic, strong) Person *person1;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"View";
    self.view.backgroundColor = UIColor.whiteColor;
    
    Person *person1 = [[Person alloc] init];
    self.person1 = person1;
    person1.name = @"person1";
    [person1 KVODealloc:^{
        NSLog(@"person1 对象已释放");
    }];
    
    Person *person2 = [[Person alloc] init];
    person2.name = @"person2";
    [person2 KVODealloc:^{
        NSLog(@"person2 对象已释放");
    }];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    [button setTitle:@"点我" forState:UIControlStateNormal];
    button.backgroundColor = UIColor.redColor;
    [button setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 75, 35);
    button.center = self.view.center;
    [button addTarget:self action:@selector(buttonEvent:) forControlEvents:UIControlEventTouchUpInside];
    [button KVODealloc:^{
        NSLog(@"button 对象已释放");
    }];
    [self.view addSubview:button];
    
    [Person addObserver:self forKeyPath:@"age" options:NSKeyValueObservingOptionNew context:nil];
}


- (void)buttonEvent:(UIButton *)button {
    self.person1 = nil;
    [button removeFromSuperview];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    NSLog(@"%s", __func__);
    Person.age = @"15";
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    NSLog(@"%@----%@", keyPath, object);
}

@end
