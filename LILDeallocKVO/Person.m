//
//  Person.m
//  LILDeallocKVO
//
//  Created by LL on 2022/7/22.
//

#import "Person.h"

@implementation Person

static NSString *_age;

- (void)dealloc {
    NSLog(@"%s, name: %@", __func__, self.name);
}

+ (void)setAge:(NSString *)age {
    _age = age;
}

+ (NSString *)age {
    return _age;
}

@end
