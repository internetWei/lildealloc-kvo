//
//  NSObject+LILDealloc.h
//  LILDeallocKVO
//
//  Created by LL on 2022/7/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (LILDealloc)

- (void)KVODealloc:(void (^ _Nullable)(void))block;

- (void)KVODealloc2:(void (^ _Nullable)(id obj))block;

@end

NS_ASSUME_NONNULL_END
