//
//  Person.h
//  LILDeallocKVO
//
//  Created by LL on 2022/7/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Person : NSObject

@property (nonatomic, copy) NSString *name;

@property (nonatomic, class) NSString *age;

@end

NS_ASSUME_NONNULL_END
